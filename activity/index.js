// Quiz
// 1. How do you create arrays in JS?

let num = [1, 2, 3, 4, 5];

// 2. How do you access the first character of an array?

arrayName[0]

// 3. How do you access the last character of an array?

arrayName[arrayName.length-1]

// 4. What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.

const indexOfTwo = num.indexOf(2); 
const indexOfFive = num.indexOf(5); 


// 5. What array method loops over all elements of an array, performing a user-defined function on each iteration?

num.forEach(function(number) {
	console.log(number * 2)
})


// 6. What array method creates a new array with elements obtained from a user-defined function?

const doubleNumbers = num.map(function(number) {
	return number * 2;


// 7. What array method checks if all its elements satisfy a given condition?

const numbers = [10, 20, 30, 40, 50];
const allEvenNumbers = numbers.every(number => number % 2 === 0);
if (allEvenNumbers) {
  console.log("All numbers are even.");
} else {
  console.log("Not all numbers are even.");
}

// 8. What array method checks if at least one of its elements satisfies a given condition?

const hasEvenNumber = num.some(function(number) {
	return number % 2 === 0;

// 9. True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
False. 

// 10. True or False: array.slice() copies elements from original array and returns them as a new array.
True.

//===========================================================

// Function coding:
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
let students = ["John", "Joe", "Jane", "Jessie"];


 const addToEnd = (array, str) => {
 	if(typeof str === "string"){
 		array.push(str);
 		return array
 	} else {
 		return "error - can only add strings to an array"
 	}
 	
 }

// addToEnd(students, "Ryan")


// 2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.

const addToStart = (array, str) => {
 	if(typeof str === "string"){
 		array.unshift(str);
 		return array
 	} else {
 		return "error - can only add strings to an array"
 	}
 	
 }

//addToEnd (students, 045)


// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

const elementChecker = (array, str) => {
 	if(array.length !== 0){
 		return array.some(name => name === str)
 	} else {
 		return "error - passed in array is empty"
 	}
 	
 }

 // addToStart (students, "Tess")


// 4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"if every element in the array ends in the passed in character, return true. Otherwise return false.Use the students array and the character "e" as arguments when testing.Tip - string characters may be accessed by passing in an index just like in arrays.

 const checkAllStringsEnding = (array, end) => {
 	if(array.length === 0 ){
 		return "error - array must NOT be empty"
 	} else if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else if(typeof end !== "string"){
 		return "error - 2nd argument must be of data type string"
 	} else if(end.length > 1){
 		return "error - 2nd argument must be a single character"
 	} else {
 		return array.every(name => name[name.length - 1] === end)
 	}
 }

 // addToStart (students, 033)


// 5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.

const stringLengthSorter = (array) => {
	if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else {
 		array.sort((a, b) => {
 			return a.length - b.length
 		})
 		console.log(array)
 	}
}

// elementChecker (students, "Jane")


// 6. Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"return the number of elements in the array that start with the character argument, must be case-insensitiveUse the students array and the character "J" as arguments when testing.

const startsWithCounter = (array, char) => {
	if(array.length === 0 ){
 		return "error - array must NOT be empty"
 	} else if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else if(typeof char !== "string"){
 		return "error - 2nd argument must be of data type string"
 	} else if(char.length > 1){
 		return "error - 2nd argument must be a single character"
 	} else {
 		let count = 0;

 		array.forEach(name => {
 			if(name[0].toLowerCase() === char.toLowerCase()){
 				count++
 			}
 		})
 		console.log(count)
 	}
}

// elementChecker ([], "Jane")


// 7. Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:if array is empty, return "error - array must NOT be empty"if at least one array element is NOT a string, return "error - all array elements must be strings"if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitiveUse the students array and the string "jo" as arguments when testing.

const likeFinder = (array, char) => {
	if(array.length === 0 ){
 		return "error - array must NOT be empty"
 	} else if(array.some(element => typeof element !== "string")){
 		return "error = all array elements must be strings"
 	} else if(typeof char !== "string"){
 		return "error - 2nd argument must be of data type string"
 	} else {
 		let find = [];

 		array.forEach(name => {
 			if(name.toLowerCase().includes(char.toLowerCase())){
 				find.push(name)
 			}
 		})
 		console.log(find)
 	}
}

// checkAllStringsEnding( students, "e")
// checkAllStringsEnding ([], "e")
// checkAllStringsEnding (["Jane", 02], "e")
// checkAllStringsEnding (students, "el")
// checkAllStringsEnding (students, 4)



// 8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.

const randomPicker = (array) => {
	console.log(array[Math.floor(Math.random() * array.length)])
}

// stringLengthSorter(students)
// stringLengthSorter([037, "John", 039, "Jane"])

// startsWithCounter(students,"j")
// likeFinder(students, "jo")






// randomPicker(students)

